import React from "react";
import { Layout, Menu, Breadcrumb } from "antd";

export default function ThongTinTaiKhoan() {
  const { Header, Content, Footer } = Layout;

  return (
    <Layout className="layout userProfile--layout">
      <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["2"]}>
          <Menu.Item key="1">nav 1</Menu.Item>
          <Menu.Item key="2">nav 2</Menu.Item>
          <Menu.Item key="3">nav 3</Menu.Item>
        </Menu>
      </Header>
      <Content style={{ padding: "0 50px" }}>
        <h1>THÔNG TIN TÀI KHOẢN</h1>
        <div className="site-layout-content">Content</div>
      </Content>
      <Footer style={{ textAlign: "center" }}>
        <h1>CYBER-MOVIE</h1>
      </Footer>
    </Layout>
  );
}
