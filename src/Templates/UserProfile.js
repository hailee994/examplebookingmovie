import { Route } from "react-router-dom";

export const UserProfile = (props) => {
  const { Component, ...restParams } = props;

  return (
    <>
      <Route
        {...restParams}
        render={(...propsRoute) => {
          return <Component {...propsRoute} />;
        }}
      />
    </>
  );
};
